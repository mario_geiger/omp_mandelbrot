all:
	g++ -fopenmp -march=native -O3 -o mandelbrot mandelbrot.cc -ljpeg

run:
	./mandelbrot mandelbrot.jpeg 1000 1000 2 2 -0.74364388703715 0.13182590420531 1e-5
