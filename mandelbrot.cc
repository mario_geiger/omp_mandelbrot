#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <jpeglib.h>
#include <omp.h>

#include <iostream>
using namespace std;

GLOBAL(void)
write_JPEG_file (const char * filename, int quality, JSAMPLE * image_buffer, int image_height, int image_width);

int main(int argc, char* argv[])
{
	if (argc < 8) {
		cout << argv[0] << " file w(px) h(px) it_max(1,2,3...) radius(2.0) x y scale" << endl;
		return 0;
	}
	/*
	 * 3000x3000
	 * -0.74364388703715;0.13182590420531
	 * 0.001x0.001
	 * real	0m5.049s
	 * user	0m18.805s
	 * sys	0m0.028s
	 */
	int arg = 2;
	const int w = atoi(argv[arg++]);
	const int h = atoi(argv[arg++]);
	JSAMPLE* image = new JSAMPLE[3*w*h];
	
	const int it_max = 0x100 * atoi(argv[arg++]);
	const double radius = atof(argv[arg++]);
	const double inf = radius * radius;
	const double cx = atof(argv[arg++]); //-0.74364388703715;
	const double cy = atof(argv[arg++]); //0.13182590420531;
	const double z = atof(argv[arg++]);
	const double 
		left   = cx-z*w/h, 
		right  = cx+z*w/h, 
		top    = cy+z, 
		bottom = cy-z;
	
	const int nth = omp_get_num_procs();
	double* a = new double[nth*w];
	double* b = new double[nth*w];
	double* x = new double[nth*w];
	int* it = new int[nth*w];
	
	#pragma omp parallel for schedule(dynamic) num_threads(nth)
	for (int j = 0; j < h; ++j) {
		const int p = w * omp_get_thread_num();
		const double y = top + (bottom - top) * j / h;
		
		for (int i = 0; i < w; ++i) {
			x[p+i] = left + (right - left) * i / w;
			a[p+i] = x[i];
			b[p+i] = y;
			it[p+i] = 0;
		}

		for (int k = 0; k < it_max; ++k)
		for (int i = 0; i < w; ++i) {
			const double a2 = a[p+i]*a[p+i];
			const double b2 = b[p+i]*b[p+i];
			b[p+i] = 2.0 * a[p+i] * b[p+i] + y;
			a[p+i] = a2 - b2 + x[p+i];
			it[p+i] += a2 + b2 < inf ? 1 : 0;
		}
		
		for (int i = 0; i < w; ++i) {
			const int px = w * j + i;
			image[3*px+0] = it[p+i] & 0xff;
			image[3*px+1] = it[p+i] & 0xff;
			image[3*px+2] = it[p+i] & 0xff;
		}
	}
		
	delete[] a;
	delete[] b;
	delete[] x;
	delete[] it;

	write_JPEG_file(argv[1], 90, image, h, w);
	delete[] image;

	return 0;
}

/*
 * Sample routine for JPEG compression.  We assume that the target file name
 * and a compression quality factor are passed in.
 */

GLOBAL(void)
write_JPEG_file (const char * filename, int quality, JSAMPLE * image_buffer, int image_height, int image_width)
{
  /* This struct contains the JPEG compression parameters and pointers to
   * working space (which is allocated as needed by the JPEG library).
   * It is possible to have several such structures, representing multiple
   * compression/decompression processes, in existence at once.  We refer
   * to any one struct (and its associated working data) as a "JPEG object".
   */
  struct jpeg_compress_struct cinfo;
  /* This struct represents a JPEG error handler.  It is declared separately
   * because applications often want to supply a specialized error handler
   * (see the second half of this file for an example).  But here we just
   * take the easy way out and use the standard error handler, which will
   * print a message on stderr and call exit() if compression fails.
   * Note that this struct must live as long as the main JPEG parameter
   * struct, to avoid dangling-pointer problems.
   */
  struct jpeg_error_mgr jerr;
  /* More stuff */
  FILE * outfile;		/* target file */
  JSAMPROW row_pointer[1];	/* pointer to JSAMPLE row[s] */
  int row_stride;		/* physical row width in image buffer */

  /* Step 1: allocate and initialize JPEG compression object */

  /* We have to set up the error handler first, in case the initialization
   * step fails.  (Unlikely, but it could happen if you are out of memory.)
   * This routine fills in the contents of struct jerr, and returns jerr's
   * address which we place into the link field in cinfo.
   */
  cinfo.err = jpeg_std_error(&jerr);
  /* Now we can initialize the JPEG compression object. */
  jpeg_create_compress(&cinfo);

  /* Step 2: specify data destination (eg, a file) */
  /* Note: steps 2 and 3 can be done in either order. */

  /* Here we use the library-supplied code to send compressed data to a
   * stdio stream.  You can also write your own code to do something else.
   * VERY IMPORTANT: use "b" option to fopen() if you are on a machine that
   * requires it in order to write binary files.
   */
  if ((outfile = fopen(filename, "wb")) == NULL) {
    fprintf(stderr, "can't open %s\n", filename);
    exit(1);
  }
  jpeg_stdio_dest(&cinfo, outfile);

  /* Step 3: set parameters for compression */

  /* First we supply a description of the input image.
   * Four fields of the cinfo struct must be filled in:
   */
  cinfo.image_width = image_width; 	/* image width and height, in pixels */
  cinfo.image_height = image_height;
  cinfo.input_components = 3;		/* # of color components per pixel */
  cinfo.in_color_space = JCS_RGB; 	/* colorspace of input image */
  /* Now use the library's routine to set default compression parameters.
   * (You must set at least cinfo.in_color_space before calling this,
   * since the defaults depend on the source color space.)
   */
  jpeg_set_defaults(&cinfo);
  /* Now you can set any non-default parameters you wish to.
   * Here we just illustrate the use of quality (quantization table) scaling:
   */
  jpeg_set_quality(&cinfo, quality, TRUE /* limit to baseline-JPEG values */);

  /* Step 4: Start compressor */

  /* TRUE ensures that we will write a complete interchange-JPEG file.
   * Pass TRUE unless you are very sure of what you're doing.
   */
  jpeg_start_compress(&cinfo, TRUE);

  /* Step 5: while (scan lines remain to be written) */
  /*           jpeg_write_scanlines(...); */

  /* Here we use the library's state variable cinfo.next_scanline as the
   * loop counter, so that we don't have to keep track ourselves.
   * To keep things simple, we pass one scanline per call; you can pass
   * more if you wish, though.
   */
  row_stride = image_width * 3;	/* JSAMPLEs per row in image_buffer */

  while (cinfo.next_scanline < cinfo.image_height) {
    /* jpeg_write_scanlines expects an array of pointers to scanlines.
     * Here the array is only one element long, but you could pass
     * more than one scanline at a time if that's more convenient.
     */
    row_pointer[0] = & image_buffer[cinfo.next_scanline * row_stride];
    (void) jpeg_write_scanlines(&cinfo, row_pointer, 1);
  }

  /* Step 6: Finish compression */

  jpeg_finish_compress(&cinfo);
  /* After finish_compress, we can close the output file. */
  fclose(outfile);

  /* Step 7: release JPEG compression object */

  /* This is an important step since it will release a good deal of memory. */
  jpeg_destroy_compress(&cinfo);

  /* And we're done! */
}


/*
 * SOME FINE POINTS:
 *
 * In the above loop, we ignored the return value of jpeg_write_scanlines,
 * which is the number of scanlines actually written.  We could get away
 * with this because we were only relying on the value of cinfo.next_scanline,
 * which will be incremented correctly.  If you maintain additional loop
 * variables then you should be careful to increment them properly.
 * Actually, for output to a stdio stream you needn't worry, because
 * then jpeg_write_scanlines will write all the lines passed (or else exit
 * with a fatal error).  Partial writes can only occur if you use a data
 * destination module that can demand suspension of the compressor.
 * (If you don't know what that's for, you don't need it.)
 *
 * If the compressor requires full-image buffers (for entropy-coding
 * optimization or a multi-scan JPEG file), it will create temporary
 * files for anything that doesn't fit within the maximum-memory setting.
 * (Note that temp files are NOT needed if you use the default parameters.)
 * On some systems you may need to set up a signal handler to ensure that
 * temporary files are deleted if the program is interrupted.  See libjpeg.doc.
 *
 * Scanlines MUST be supplied in top-to-bottom order if you want your JPEG
 * files to be compatible with everyone else's.  If you cannot readily read
 * your data in that order, you'll need an intermediate array to hold the
 * image.  See rdtarga.c or rdbmp.c for examples of handling bottom-to-top
 * source data using the JPEG code's internal virtual-array mechanisms.
 */


